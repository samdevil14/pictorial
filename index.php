<?php include_once("header.php"); ?>
        <div class="text-center">
          <h1>Pictorial Magic Productions</h1>
        </div>

        <p class="text-center text-uppercase" style="font-size: 25px;">Crafted with passion</p>
        <div class="row">
          <div class="col-4"><hr class="hrleft"></div>
          <div class="col-3">
            <p class="text-center" style="font-size:25px;color:#fff;">Our Mission</p>
          </div>
          <div class="col-4"><hr class="hrright"></div>
        </div>

        <div>
          <p class="text-center" style="font-size:25px;color:#fff;">To make visual communication easy and affordable and value driven</p>
        </div>

        <div class="row">
          <div class="col-4"><hr class="hrleft"></div>
          <div class="col-3">
            <p class="text-center" style="font-size:25px;color:#fff;">Our Vision</p>
          </div>
          <div class="col-4"><hr class="hrright"></div>
        </div>

        <div>
          <p class="text-center" style="font-size:25px;color:#fff;">To be known as content and quality driven advertising and film</p>
        </div>

        <div>
          <p class="text-center passiontext">We are passionate to transform your vision through our Visuals</p>
        </div>
        
        <div class="text-center">
          <a class="btn btn-light" style="border-radius:360px;">SEE HOW</a>
        </div>
      </div>
    
<?php include_once("footer.php"); ?>