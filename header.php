<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=EB+Garamond:400,400i" rel="stylesheet">
    <title>Pictorial - Home</title>
    <style>
        .pagebg{
            background-image: url('images/bg3.jpg');
            background-size:cover;
        }
        .navbar-nav{
          margin: 0px auto;
        }
        .nav-item{
          padding-right: 2rem;
        }
        .nav-link{
          color: #fff !important;
          border: 1px solid transparent;
        }

        .nav-link:focus, .nav-link:hover{
          color: #fff !important;
          font-weight: 500;
          border: 1px solid #fff !important;
        }
        
        hr{
          border-top: 1px solid #fff;
        }

        .hrleft{
          margin-left: 37%;
        }

        .hrright{
          margin-right: 37%;
        }

        .logo{
          text-align: center;
        }

        .logo img{
          width: 200px;
        }

        .passiontext{
          font-size:2rem;
          color:#fff;
          font-family: 'EB Garamond script=all rev=1', serif;
          font-style:italic;
        }

        .context-dark, .bg-gray-dark, .bg-primary {
            color: rgba(255, 255, 255, 0.8);
        }

        .navbar-light .navbar-toggler{
          color: #fff !important;
          border-color: #fff !important;
        }
    </style>
  </head>
  <body>
    <div class="pagebg">
      <div class="container">
        <div class="logo">
          <img src="images/logo.png" alt="" srcset="">
        </div>
        <nav class="navbar navbar-expand-lg navbar-dark">
          <a class="navbar-brand text-white d-lg-none" href="#">Pictorial</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                  <a class="nav-link" href="#">Home</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Our Story</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Founder</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Services</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Process</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Contact Us</a>
                </li>
            </ul>
            
          </div>
        </nav>
        <nav class="navbar navbar-expand-sm">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </nav>