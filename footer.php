<footer class="section footer-classic context-dark bg-image" style="background: #272532;">
        <div class="container">
          <div class="row mt-4">
            <div class="col-md-4 col-xl-5 pt-4">
              <div class="pr-xl-4">
               <ul style="list-style:none;">
                 <li class="mb-2">Home</li>
                 <li class="mb-2">About us</li>
                 <li class="mb-2">Services</li>
                 <li class="mb-2">Contact Us</li>
               </ul>
              </div>
            </div>
            <div class="col-md-4 pt-4">
              <h5>Contact Us</h5>
              <dl class="contact-list">
                <dt>pictorialmagic@gmail.com</dt>
                <dd>+91 7021278115</dd>
              </dl>
            </div>
            <div class="col-md-4 col-xl-3 pt-4">
              <h5>Sign up for Newsletter</h5>
              <input type="text" class="form-control" placeholder="Email Id" style="background: #22202b;border: 1px solid #22202b;">
              <div class="pt-2">
                <a class="btn btn-primary">SIGN UP</a>
              </div>
              
            </div>
          </div>
        </div>
      </footer>
    </div>
      <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>